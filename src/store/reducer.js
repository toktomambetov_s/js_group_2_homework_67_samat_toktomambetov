const correctPassword = '7263';

const initialStore = {
    password: '',
    passwordMatch: false,
};

const reducer = (state = initialStore, action) => {
    if (action.type === 'ADD_NUMBER') {
        return {...state, password: state.password + action.number}
    }
    if (action.type === 'REMOVE_NUMBER') {
        return {...state, password: state.password.slice(0, -1)}
    }
    if (action.type === 'ENTER_PASSWORD') {
        if (state.password === correctPassword) {
            return {...state, password: 'Access Granted', passwordMatch: true}
        }
    }
    return state;
};

export default reducer;