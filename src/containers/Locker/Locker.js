import React, { Component } from 'react';
import './Locker.css';

import {connect} from 'react-redux';

class Locker extends Component {

    addNumberHandler = e => {
        e.preventDefault();
        if (this.props.password.length < 4) {
            e.preventDefault();
            this.props.addNumber(e.target.value);
        }
    };

    removeNumberHandler = e => {
        e.preventDefault();
        this.props.removeNumber();
    };

    enterPasswordHandler = e => {
        e.preventDefault();
        this.props.enterPassword();
    };

    hiddenPassword = () => {
        if (this.props.passwordMatch) {
            return this.props.password;
        } else {
            const hiddenPassword = new Array(this.props.password.length).fill('*').join('');
            return hiddenPassword;
        }

    };

    render() {
        return (
            <div className="Locker">
                <form>
                    <span className={this.props.passwordMatch ? "Password Green" : "Password"}>{this.hiddenPassword()}</span>
                    <button value="7" onClick={this.addNumberHandler}>7</button>
                    <button value="8" onClick={this.addNumberHandler}>8</button>
                    <button value="9" onClick={this.addNumberHandler}>9</button>
                    <button value="4" onClick={this.addNumberHandler}>4</button>
                    <button value="5" onClick={this.addNumberHandler}>5</button>
                    <button value="6" onClick={this.addNumberHandler}>6</button>
                    <button value="1" onClick={this.addNumberHandler}>1</button>
                    <button value="2" onClick={this.addNumberHandler}>2</button>
                    <button value="3" onClick={this.addNumberHandler}>3</button>
                    <button onClick={this.removeNumberHandler}>C</button>
                    <button value="0" onClick={this.addNumberHandler}>0</button>
                    <button onClick={this.enterPasswordHandler}>E</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        password: state.password,
        passwordMatch: state.passwordMatch
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: (number) => dispatch({type: 'ADD_NUMBER', number: number}),
        removeNumber: () => dispatch({type: 'REMOVE_NUMBER'}),
        enterPassword: () => dispatch({type: 'ENTER_PASSWORD'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Locker);